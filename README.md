# Vision Statement

CuteHMI is meant to be a set of software components (libraries and executables)
targeted at building HMI (Human Machine Interface) applications.

These components aim to fill the gap, between open-source editions of Qt and
commercial software targeted at physical computing (HMI/SCADA/IoT/BMS/RMS/etc)
build around it, thus providing complete open-source suite for such types of
applications for home and industrial use.

Example use case is to create user interface that can be used to communicate
with PLC (programmable logic controller) to turn on air conditioning in the
building for example. 



